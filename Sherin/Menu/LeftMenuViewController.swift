//
//  LeftMenuViewController.swift
//  Sherin
//
//  Created by Admin on 5/2/16.
//  Copyright © 2016 mss. All rights reserved.
//

import UIKit

protocol MenuItemsFilter {
    var menuItems: Array<(String, Selector)> {get set}
}

class LeftMenuViewController: UITableViewController, InstanceTrait, LoginDelegate {
    
    @IBOutlet weak var userRoleLabel: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    lazy var menuItems = [(String, Selector)]()
    var navController: UINavigationController!
    var loginItem: (String, Selector) = UserData.sharedData.role == .guest ? ("Login", #selector(LeftMenuViewController.openLogin)) : ("Logout", #selector(LeftMenuViewController.logout))
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(LeftMenuViewController.tokenOutdated), name: "TokenOutdated", object: nil)
        self.navController = menuContainerViewController.centerViewController as! UINavigationController
        reloadData()
    }
    
    func reloadData() {
        self.userRoleLabel.text = locStr(UserData.sharedData.role.rawValue)
        if let name = UserData.sharedData.name {
            self.userNameLabel.text = name
        } else {
            self.userNameLabel.text = ""
        }
        loginItem = UserData.sharedData.role == .guest ? ("Login", #selector(LeftMenuViewController.openLogin)) : ("Logout", #selector(LeftMenuViewController.logout))
        menuItems = [
            ("Dashboard", #selector(LeftMenuViewController.openDashboard) /*DashboardViewController.instance()*/),
            ("Catalogue", #selector(LeftMenuViewController.openCatalogue) /*CatalogueViewController.instance()*/),
            ("News", #selector(LeftMenuViewController.openNews) /*NewsViewController.instance()*/),
            ("Map", #selector(LeftMenuViewController.openMap) /*MapViewController.instance()*/),
            ("Advices", #selector(LeftMenuViewController.openAdvices) /*AdvicesViewController.instance()*/),
            ("ContactUs", #selector(LeftMenuViewController.openContactUs) /*ContactUsViewController.instance()*/),
            ("Suggestions", #selector(LeftMenuViewController.openSuggest) /*SuggestionsViewController.instance()*/),
            loginItem
        ]
        filterMenuItems()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuItems.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("MenuCell", forIndexPath: indexPath) 
        cell.textLabel?.text = locStr(menuItems[indexPath.row].0)
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        performSelector(menuItems[indexPath.row].1)
        //navController.setViewControllers([menuItems[indexPath.row].1], animated: true)
        
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func openDashboard() {
        navController.setViewControllers([DashboardViewController.instance()], animated: false)
        menuContainerViewController.toggleLeftSideMenuCompletion(nil)
    }
    
    func openSuggest() {
        navController.setViewControllers([SuggestionsViewController.instance()], animated: false)
        menuContainerViewController.toggleLeftSideMenuCompletion(nil)
    }
    
    func openContactUs() {
        navController.setViewControllers([ContactUsViewController.instance()], animated: false)
        menuContainerViewController.toggleLeftSideMenuCompletion(nil)
    }
    
    
    func openAdvices() {
        navController.setViewControllers([AdvicesViewController.instance()], animated: false)
        menuContainerViewController.toggleLeftSideMenuCompletion(nil)
    }
    
    func openMap() {
        navController.setViewControllers([MapViewController.instance()], animated: true)
        menuContainerViewController.toggleLeftSideMenuCompletion(nil)
    }
    
    func openCatalogue() {
        navController.setViewControllers([CatalogueViewController.instance()], animated: false)
        menuContainerViewController.toggleLeftSideMenuCompletion(nil)
    }
    
    func openNews() {
        navController.setViewControllers([NewsViewController.instance()], animated: true)
        menuContainerViewController.toggleLeftSideMenuCompletion(nil)
    }
    
    func openLogin() {
        let loginNavController = LoginViewController.instance() as! UINavigationController
        let loginVC = loginNavController.childViewControllers[0] as! LoginViewController
        loginVC.delegate = self
        navController.presentViewController(loginNavController, animated: true, completion: nil)
    }
    
    func tokenOutdated() {
        UserData.sharedData.dropData()
        self.loginItem = ("Login", #selector(LeftMenuViewController.openLogin))
        self.reloadData()
        self.tableView.reloadData()
        
        self.openLogin()
    }
    
    func logout() {
        API.logout({ (reply) in
            UserData.sharedData.dropData()
            self.loginItem = ("Login", #selector(LeftMenuViewController.openLogin))
            self.reloadData()
            self.tableView.reloadData()
            self.openDashboard()
            }) { (reply) in
                // FAILURE
        }
    }
    
    func afterLogin() {
        self.reloadData()
        self.tableView.reloadData()
        openDashboard()
    }
    
    // MARK: - MenuItems Filters
    
    func filterMenuItems() {
        switch UserData.sharedData.role {
        case .guest:
            removeWithTitles(["Map", "Advices", "ContactUs"])
        case .seller:
            removeWithTitles(["Advices", "ContactUs"])
        case .client:
            removeWithTitle("Map")
        }
    }
    
    func removeWithTitles(titles: [String]) {
        for title in titles {
            removeWithTitle(title)
        }
    }
    
    func removeWithTitle(title: String) {
        menuItems.removeAtIndex(menuItems.indexOf({
            item in
            return item.0 == title
        })!)
    }
    
}
