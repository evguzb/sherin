//
//  API.swift
//  Sherin
//
//  Created by admin on 6/10/16.
//  Copyright © 2016 mss. All rights reserved.
//

import Foundation
import Alamofire
import SVProgressHUD



class API {
    static let baseURL = "http://sherin.mss.uz/api/"
    
    static let dateFormat = "yyyy-MM-dd HH-mm-ss"
    static let dateFormatShort = "dd.MM.yy"
    // Image folders
    static let brand_bg = "http://sherin.mss.uz/img/brand_bg/"
    static let brand_logo = "http://sherin.mss.uz/img/brand_logo/"
    static let type_img = "http://sherin.mss.uz/img/type_img/"
    static let product_img = "http://sherin.mss.uz/img/product_img/"
    static let news_preview = "http://sherin.mss.uz/img/news_preview/"
    static let advice_preview = "http://sherin.mss.uz/img/advice_preview/"
    static let advice_image = "http://sherin.mss.uz/img/advice_images/"
    static let news_image = "http://sherin.mss.uz/img/news_images/"
    //
    
    static func getBrands(onSuccess:([Brand]) -> Void, onFailure:(Reply) -> Void) {
        let request = Request(type: .brands)
        executeRequest(request) { reply in
            if reply.status {
                let brands = List<Brand>.initWith(reply, parent: nil).items
                onSuccess(brands)
            } else {
                onFailure(reply)
            }
        }
    }
    
    static func getNews(onSuccess:([NewsItem]) -> Void, onFailure:(Reply) -> Void) {
        let request = Request(type: .news)
        executeRequest(request) { (reply) in
            if reply.status {
                let news = List<NewsItem>.initWith(reply, parent: nil).items
                onSuccess(news)
            } else {
                onFailure(reply)
            }
        }
    }

    static func getMapLocation(onSuccess:([MapLocation]) -> Void, onFailure:(Reply) -> Void) {
        let request = Request(type: .outlets)
        request.setParam(UserData.sharedData.token!, forKey: "token")
        request.setParam(UserData.sharedData.id!, forKey: "id")
        executeRequest(request) { (reply) in
            if reply.status {
                let mapLocations = List<MapLocation>.initWith(reply, parent: nil).items
                onSuccess(mapLocations)
            } else {
                onFailure(reply)
            }
        }
    }
    
    static func getAdvices(onSuccess: ([Advice] -> Void), onFailure: (Reply) -> Void) {
        let request = Request(type: .advices)
        request.setParam(UserData.sharedData.id!, forKey: "id")
        request.setParam(UserData.sharedData.token!, forKey: "token")
        executeRequest(request) { (reply) in
            if reply.status {
                let advices = List<Advice>.initWith(reply, parent: nil).items
                onSuccess(advices)
            } else {
                onFailure(reply)
            }
        }
    }
    
    static func getOffers(onSuccess: ([Offer] -> Void), onFailure: (Reply) -> Void) {
        let request = Request(type: .offers)
        request.setParam(UserData.sharedData.deviceToken, forKey: "device_token")
        executeRequest(request) { (reply) in
            if reply.status {
                let offers = List<Offer>.initWith(reply, parent: nil).items
                onSuccess(offers)
            } else {
                onFailure(reply)
            }
        }
    }
    
    static func authDevice(onSuccess onSuccess: (Reply) -> Void, onFailure: (Reply) -> Void) {
        let request = Request(type: .authDevice)
        request.setParam(UserData.sharedData.device_type, forKey: "device_type")
        /// FIX IT
        request.setParam(UserData.sharedData.deviceToken, forKey: "device_token")
        executeRequest(request) { reply in
            if reply.status {
                onSuccess(reply)
            } else {
                onFailure(reply)
            }
        }
        
    }

    static func login(login login: String, password: String, callback: (Reply) -> Void) {
        let request = Request(type: .login)
        request.setParam(login, forKey: "email")
        request.setParam(password, forKey: "password")
        request.setParam(UserData.sharedData.server_device_token!, forKey: "server_device_token")
        executeRequest(request) { (reply) in
            callback(reply)
        }
    }
    
    static func logout(onSuccess: (Reply) -> Void, onFailure: (Reply) -> Void) {
        let request = Request(type: .logout)
        request.setParam(UserData.sharedData.id!, forKey: "id")
        request.setParam(UserData.sharedData.token!, forKey: "token")
        executeRequest(request) { (reply) in
            if reply.status {
                onSuccess(reply)
            } else {
                onFailure(reply)
            }
        }
    }
    
    static func sendContact(title title: String, content: String, onSuccess: (Reply) -> Void, onFailure: (Reply) -> Void) {
        let request = Request(type: .contact)
        request.setParam(UserData.sharedData.id!, forKey: "id")
        request.setParam(UserData.sharedData.token!, forKey: "token")
        request.setParam(title, forKey: "title")
        request.setParam(content, forKey: "content")
        executeRequest(request) { (reply) in
            if reply.status {
                onSuccess(reply)
            } else {
                onFailure(reply)
            }
        }
    }
    
    static func executeRequest(request: Request, callback: (Reply) -> Void) {
        SVProgressHUD.show()
        Alamofire.request(.GET, baseURL + request.type.rawValue, parameters: request.params, encoding: .URL, headers: nil).responseString(completionHandler: { (response) in
            print(response.result.value)
        }).responseJSON { response in
            SVProgressHUD.dismiss()
            if let dict = response.result.value as? [String: AnyObject] {
                let reply = Reply(dict: dict)
                if reply.code == 2 {
                    // auth problem
                    NSNotificationCenter.defaultCenter().postNotificationName("TokenOutdated", object: nil)
                }
                callback(reply)
            } else {
                callback(Reply(error: response.result.error!))
            }
        }
    }
    
    static func executeRequestWithoutProgress(request: Request, callback: (Reply) -> Void) {
        Alamofire.request(.GET, baseURL + request.type.rawValue, parameters: request.params, encoding: .URL, headers: nil).responseString(completionHandler: { (response) in
            print(response.result.value)
        }).responseJSON { response in
            if let dict = response.result.value as? [String: AnyObject] {
                let reply = Reply(dict: dict)
                if reply.code == 2 {
                    // auth problem
                    NSNotificationCenter.defaultCenter().postNotificationName("TokenOutdated", object: nil)
                }
                callback(reply)
            } else {
                callback(Reply(error: response.result.error!))
            }
        }
    }
    
    
}