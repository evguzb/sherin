//
//  Reply.swift
//  Sherin
//
//  Created by admin on 6/10/16.
//  Copyright © 2016 mss. All rights reserved.
//

import Foundation

class Reply {
    var data: [String: AnyObject]
    let error: NSError?
    let status: Bool
    let code: Int?
    let message: String?
    init(dict: [String: AnyObject]) {
        self.data = [:]
        self.error = nil
        self.status = dict["status"] as! Bool
        if let code = dict["code"] as? String {
            self.code = Int(code)
        } else {
            self.code = nil
        }
        self.message = dict["message"] as? String
        if self.status {
            self.data = dict
        }
    }
    init(error: NSError) {
        self.error = error
        self.data = [:]
        self.status = false
        self.message = nil
        self.code = nil
    }
}