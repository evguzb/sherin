//
//  Request.swift
//  Sherin
//
//  Created by admin on 6/10/16.
//  Copyright © 2016 mss. All rights reserved.
//

import Foundation

enum RequestType: String {
    case login = "login"
    case logout = "logout"
    case authDevice = "auth-device"
    case brands = "brands"
    case products = "products"
    case outlets = "outlets"
    case advices = "advice"
    case news = "news"
    case offers = "offers"
    case like = "like"
    case dislike = "dislike"
    case contact = "contact"
}
class Request {
    let type: RequestType
    private(set) var params = [String: AnyObject]()
    
    init (type: RequestType) {
        self.type = type
    }
    
    func setParam(param: AnyObject, forKey key: String) {
        self.params[key] = param
    }
}