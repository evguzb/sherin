//
//  LoginViewController.swift
//  Sherin
//
//  Created by admin on 6/16/16.
//  Copyright © 2016 mss. All rights reserved.
//

import UIKit

protocol LoginDelegate {
    func afterLogin()
}

class LoginViewController: UIViewController, InstanceTrait, UITextFieldDelegate {
    
    @IBOutlet weak var tipLabel: UILabel!
    @IBOutlet weak var loginTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    var delegate: LoginDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: locStr("Back"), style: .Plain, target: self, action: #selector(LoginViewController.dismiss))
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(LoginViewController.keyboardWillShow), name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(LoginViewController.keyboardDidHide), name: UIKeyboardWillHideNotification, object: nil)
        setupUIElements()
        // Do any additional setup after loading the view.
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        loginTextField.resignFirstResponder()
        passwordTextField.resignFirstResponder()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupUIElements() {
        loginTextField.delegate = self
        passwordTextField.delegate = self
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    func isFieldsReady() -> Bool {
        if loginTextField.text == "" || passwordTextField.text == "" {
            return false
        }
        return true
    }
    
    func dismiss() {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    // MARK: - Text Field Delegate
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    // MARK: - Actions
    
    @IBAction func loginTapped(sender: UIButton) {
        if isFieldsReady() {
            API.login(login: loginTextField.text!, password: passwordTextField.text!, callback: { (reply) in
                print(reply.data)
                if reply.status {
                    UserData.sharedData.setUserData(fromReply: reply)
                    //(self.menuContainerViewController.centerViewController as! UINavigationController).setViewControllers([DashboardViewController.instance()], animated: false)
                    self.delegate?.afterLogin()
                    self.dismiss()
                } else {
                    if let message = reply.message {
                        self.presentAlertWithtMessage(message)
                    }
                }
            })
        } else {
            self.presentAlertWithtMessage(locStr("FieldsNotReady"))
        }
        
    }
    
    // MARK: - on Keyboard appearence
    
    func keyboardWillShow() {
        self.navigationController?.view.frame.origin.y = -80
    }
    
    func keyboardDidHide() {
        self.navigationController?.view.frame.origin.y = 0
    }
    
    // MARK: - Alerts
    
    func presentAlertWithtMessage(message: String) {
        let alertController = UIAlertController(title: nil, message: message, preferredStyle: .Alert)
        alertController.addAction(UIAlertAction(title: "Ок", style: .Default, handler: nil))
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
}
