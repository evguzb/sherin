//
//  DashboardCollectionViewCell.swift
//  Sherin
//
//  Created by admin on 6/15/16.
//  Copyright © 2016 mss. All rights reserved.
//

import UIKit

class DashboardCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var logoImageView: UIImageView!
    
    @IBOutlet weak var titleLabel: UILabel!
}
