//
//  DashboardViewController.swift
//  Sherin
//
//  Created by Admin on 5/2/16.
//  Copyright © 2016 mss. All rights reserved.
//

import UIKit
import KTCenterFlowLayout



class DashboardViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, InstanceTrait, MenuButtonProtocol, AnalyticsHandler {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    
    // Title, image, selector
    var menuItems = [(String, String, Selector)]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.leftBarButtonItem = menuBarButton()
        let centerFlowLayout = KTCenterFlowLayout()
        centerFlowLayout.minimumLineSpacing = 8.0
        centerFlowLayout.minimumInteritemSpacing = 8.0
        self.collectionView.collectionViewLayout = centerFlowLayout
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        menuItems = [
            ("Catalogue", "catalogue_dash", #selector(DashboardViewController.openCatalogue)),
            ("News", "news_dash", #selector(DashboardViewController.openNews)),
            ("Advices", "advices_dash", #selector(DashboardViewController.openAdvices)),
            ("Map", "map_dash", #selector(DashboardViewController.openMap)),
            ("Suggestions", "suggest_dash", #selector(DashboardViewController.openSuggestions)),
            ("ContactUs", "contact_dash", #selector(DashboardViewController.openContactUs))
            
        ]
        switch UserData.sharedData.role {
        case .guest:
            removeWithTitles(["Map", "Advices", "ContactUs"])
        case .seller:
            removeWithTitles(["Advices", "ContactUs"])
        case .client:
            removeWithTitle("Map")
        }
        sendScreenStats()
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Collection View
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return menuItems.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(DashboardCollectionViewCell.className(), forIndexPath: indexPath) as! DashboardCollectionViewCell
        configureCell(cell, atIndexPath: indexPath)
        return cell
    }
    
    func configureCell(cell: DashboardCollectionViewCell, atIndexPath indexPath: NSIndexPath) {
        cell.titleLabel.text = locStr(menuItems[indexPath.item].0)
        cell.logoImageView.image = UIImage(named: menuItems[indexPath.item].1)
    }
    
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        let superWidth = self.view.frame.width
        return CGSize(width: superWidth / 3.4, height: superWidth / 2.8)
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        performSelector(menuItems[indexPath.row].2)
    }
    
    func openCatalogue() {
        navigationController?.setViewControllers([CatalogueViewController.instance()], animated: true)
    }
    
    func openNews() {
        navigationController?.setViewControllers([NewsViewController.instance()], animated: true)
    }
    
    func openMap() {
        navigationController?.setViewControllers([MapViewController.instance()], animated: true)
    }
    
    func openAdvices() {
        navigationController?.setViewControllers([AdvicesViewController.instance()], animated: true)
    }
    
    func openSuggestions() {
        navigationController?.setViewControllers([SuggestionsViewController.instance()], animated: true)
    }
    
    func openContactUs() {
        navigationController?.setViewControllers([ContactUsViewController.instance()], animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - Helper Methods
    
    func removeWithTitles(titles: [String]) {
        for title in titles {
            removeWithTitle(title)
        }
    }
    
    func removeWithTitle(title: String) {
        menuItems.removeAtIndex(menuItems.indexOf({
            item in
            return item.0 == title
        })!)
    }


}
