//
//  WelcomeViewController.swift
//  Sherin
//
//  Created by Admin on 5/2/16.
//  Copyright © 2016 mss. All rights reserved.
//

import UIKit
import MFSideMenu

class WelcomeViewController: UIViewController, InstanceTrait {
    
    @IBOutlet var buttons: [UIButton]!

    override func viewDidLoad() {
        super.viewDidLoad()
        for button in buttons {
            button.addTarget(self, action: #selector(WelcomeViewController.buttonTapped(_:)), forControlEvents: .TouchUpInside)
        }
        authDevice()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func authDevice() {
        API.authDevice(onSuccess: { (reply) in
            UserData.sharedData.setDeviceData(fromReply: reply)
            }) { (reply) in
                let tryAgainAction = UIAlertAction(title: locStr("TryAgain"), style: .Default, handler: { (action) in
                    self.authDevice()
                })
                AlertHelper.presentAlert(withTitle: nil, message: "NetworkAlertMessage", actions: [tryAgainAction])
        }
    }
    
    // MARK: - Actions
    
    func buttonTapped(sender: UIButton) {
        print(sender.tag)
        let defaults = NSUserDefaults.standardUserDefaults()
        var lang: Language!
        switch sender.tag {
        case 1: lang = .russian
        case 2: lang = .uzbek
        default: break
        }
        LocalizeHelper.sharedLocale.setLanguage(lang)
        defaults.setValue(lang.rawValue, forKey: "lang")
        defaults.setBool(true, forKey: "isLaunchedBefore")
        
        let container = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("SideMenuContainerViewController") as! MFSideMenuContainerViewController
        container.centerViewController = MainNavigationController.instance()
        container.leftMenuViewController = LeftMenuViewController.instance()
        
        (UIApplication.sharedApplication().delegate as! AppDelegate).window?.rootViewController = container
    }

}
