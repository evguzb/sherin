//
//  WebViewController.swift
//  Sherin
//
//  Created by admin on 7/18/16.
//  Copyright © 2016 mss. All rights reserved.
//

import UIKit
import SVProgressHUD

class WebViewController: UIViewController, UIWebViewDelegate {
    
    var webView = UIWebView()
    var urlString: String!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addSubview(webView)
        self.webView.delegate = self
        self.title = locStr("Direction").uppercaseString
        webView.delegate = self
        webView.snp_makeConstraints { (make) -> Void in
            make.edges.equalTo(self.view).inset(UIEdgeInsetsZero)
        }
        let request = NSURLRequest(URL: NSURL(string: urlString)!)
        SVProgressHUD.show()
        self.webView.loadRequest(request)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func webViewDidFinishLoad(webView: UIWebView) {
        SVProgressHUD.dismiss()
    }
    
    func webView(webView: UIWebView, didFailLoadWithError error: NSError?) {
        SVProgressHUD.dismiss()
        if let error = error {
            AlertHelper.presentDefaultAlert(withMessage: error.localizedDescription)
        }
    }

}
