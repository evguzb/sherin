//
//  MapViewController.swift
//  Sherin
//
//  Created by Admin on 5/3/16.
//  Copyright © 2016 mss. All rights reserved.
//

import UIKit
import MapKit

class MapViewController: UIViewController, InstanceTrait, MKMapViewDelegate, MenuButtonProtocol {

    @IBOutlet weak var directionButton: UIButton!
    @IBOutlet weak var mapView: MKMapView!
    var data = [MapLocation]()
    
    var point:MKAnnotationView?
    
    let locationManager = CLLocationManager()
    
    var selectedPoint:MKAnnotationView? {
        set (newValue) {
            if newValue != nil {
                self.directionButton.hidden = false
            } else {
                self.directionButton.hidden = true
            }
            point = newValue
        } get {
            return point
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.leftBarButtonItem = menuBarButton()
        directionButton.setTitle(locStr("GetDirection"), forState: .Normal)
        
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        
        mapView.delegate = self
        mapView.showsUserLocation = true
        
        API.getMapLocation({ (mapLocations) in
            self.data.removeAll()
            self.data.appendContentsOf(mapLocations)
            self.mapView.addAnnotations(self.data)
            self.mapView.showAnnotations(self.data, animated: true)
            }) { (reply) in

        }

    }

    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        self.locationManager.stopUpdatingLocation()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func mapView(mapView: MKMapView, didUpdateUserLocation userLocation: MKUserLocation) {
        print("updated")
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func directionButtonTapped(sender: UIButton) {
        let mapItem = MKMapItem(placemark: MKPlacemark(coordinate: self.selectedPoint!.annotation!.coordinate, addressDictionary: nil))
        let urlString = "https://www.google.com/maps/?saddr=\(mapView.userLocation.coordinate.latitude),\(mapView.userLocation.coordinate.longitude)&daddr=\(mapItem.placemark.coordinate.latitude),\(mapItem.placemark.coordinate.longitude)"
        let webViewController = WebViewController()
        webViewController.urlString = urlString
        self.navigationController?.pushViewController(webViewController, animated: true)
        //getDirections(mapItem)
    }
    
    // MARK: - MKMapView Delegate
    
    func mapView(mapView: MKMapView, didSelectAnnotationView view: MKAnnotationView) {
        self.selectedPoint = view
    }
    
    func mapView(mapView: MKMapView, didDeselectAnnotationView view: MKAnnotationView) {
        self.selectedPoint = nil
    }
    
    /*
    func getDirections(destination:MKMapItem) {
        
        let request = MKDirectionsRequest()
        request.source = MKMapItem.mapItemForCurrentLocation()
        request.destination = destination
        request.requestsAlternateRoutes = false
        
        let directions = MKDirections(request: request)
        
        directions.calculateDirectionsWithCompletionHandler { (response, error) -> Void in
            if error != nil {
                print("Error getting directions", terminator: "")
            } else {
                self.showRoute(response!)
            }
        }
    }
    
    
    func showRoute(response: MKDirectionsResponse) {
        
        for route in response.routes {
            
            self.mapView.addOverlay(route.polyline,
                                    level: MKOverlayLevel.AboveRoads)
            
            for step in route.steps {
                print(step.instructions)
            }
        }
        
        if let userLocation = self.mapView.userLocation.location {
            let region = MKCoordinateRegionMakeWithDistance(userLocation.coordinate, 2000, 2000)
            self.mapView.setRegion(region, animated: true)
        }
        
    }
    
    func mapView(mapView: MKMapView, rendererForOverlay
        overlay: MKOverlay) -> MKOverlayRenderer {
        let renderer = MKPolylineRenderer(overlay: overlay)
        
        renderer.strokeColor =  UIColor(red: 224/255, green: 72/255, blue: 77/255, alpha: 1.0)
        renderer.lineWidth = 5.0
        return renderer
    }
     */

}

extension MapViewController : CLLocationManagerDelegate {

}
