//
//  AdviceTableViewCell.swift
//  Sherin
//
//  Created by admin on 7/7/16.
//  Copyright © 2016 mss. All rights reserved.
//

import UIKit

class AdviceTableViewCell: UITableViewCell {
    
    @IBOutlet weak var previewImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
