//
//  AdvicesViewController.swift
//  Sherin
//
//  Created by Admin on 5/3/16.
//  Copyright © 2016 mss. All rights reserved.
//

import UIKit

class AdvicesViewController: UITableViewController, InstanceTrait, MenuButtonProtocol {
    
    var advices = [Advice]()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        self.navigationItem.leftBarButtonItem = menuBarButton()
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 100.0
        API.getAdvices({ (advices) in
            print(advices)
            self.advices = advices
            self.tableView.reloadData()
            }) { (reply) in
                
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.advices.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(AdviceTableViewCell.className()  , forIndexPath: indexPath) as! AdviceTableViewCell
        configureCell(cell, forRow: indexPath.row)
        return cell
    }
    
    func configureCell(cell: AdviceTableViewCell, forRow row: Int) {
        if let url = NSURL(string: API.advice_preview + self.advices[row].previewImg) where self.advices[row].previewImg != "" {
            cell.previewImageView.sd_setImageWithURL(url)
        }
        cell.titleLabel.text = self.advices[row].title
    }

    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let adviceInnerVC = AdvicesInnerViewController.instance() as! AdvicesInnerViewController
        adviceInnerVC.advice = self.advices[indexPath.row]
        navigationController?.pushViewController(adviceInnerVC, animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
