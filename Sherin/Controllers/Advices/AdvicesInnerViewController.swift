//
//  AdvicesInnerViewController.swift
//  Sherin
//
//  Created by admin on 7/8/16.
//  Copyright © 2016 mss. All rights reserved.
//

import UIKit

class AdvicesInnerViewController: UITableViewController, InstanceTrait {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!

    var advice: Advice!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = self.view.frame.height
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        setupUI()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        //setupUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: Scroll View Delegate
    
    override func scrollViewDidScroll(scrollView: UIScrollView) {
        if scrollView == self.scrollView {
            if scrollView.contentOffset.x % self.view.frame.width == 0 {
                self.pageControl.currentPage = Int(scrollView.contentOffset.x / self.view.frame.width)
            }
        }
    }

    
    // MARK: UI
    
    func setupUI() {
        self.scrollView.delegate = self
        self.pageControl.numberOfPages = self.advice.images.count
        self.pageControl.currentPage = 0
        addImageViewsToScrollView()
        
        titleLabel.text = self.advice.title
        contentLabel.text = self.advice.content
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = API.dateFormatShort
        dateLabel.text = dateFormatter.stringFromDate(self.advice.date)
    }
    
    func addImageViewsToScrollView() {
        let width = Double(self.view.frame.width)
        var count = 0.0
        for image in self.advice.images {
            let imageView = UIImageView(frame: CGRect(x: 0.0 + width * count, y: 0.0, width: width, height: Double(self.scrollView.frame.height)))
            if let url = NSURL(string: API.advice_image + image.img) where image.img != "" {
                imageView.sd_setImageWithURL(url)
            } else {
                imageView.image = UIImage(named: "logo")
            }
            count += 1
            self.scrollView.addSubview(imageView)
            
        }
        self.scrollView.contentSize = CGSize(width: width * count, height: Double(self.scrollView.frame.height))
    }

}
