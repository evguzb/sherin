//
//  NewsViewController.swift
//  Sherin
//
//  Created by Admin on 5/3/16.
//  Copyright © 2016 mss. All rights reserved.
//

import UIKit

class NewsViewController: UITableViewController, InstanceTrait, MenuButtonProtocol {
    
    var news = [NewsItem]()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.clearsSelectionOnViewWillAppear = false
        self.navigationItem.leftBarButtonItem = menuBarButton()
        API.getNews({ (news) in
            self.news = news
            self.tableView.reloadData()
            }) { (reply) in
                
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.news.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(NewsTableViewCell.className(), forIndexPath: indexPath) as! NewsTableViewCell

        configureCell(cell, forRow: indexPath.row)

        return cell
    }
    
    func configureCell(cell: NewsTableViewCell, forRow row: Int) {
        cell.titleLabel.text = self.news[row].title
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = API.dateFormatShort
        cell.dateLabel.text = dateFormatter.stringFromDate(self.news[row].date)
        
        if let url = NSURL(string: API.news_preview + self.news[row].previewImg) where
            self.news[row].previewImg != "" {
            cell.bgImageView.sd_setImageWithURL(url)
        }
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let newsInnerVC = NewsInnerViewController.instance() as! NewsInnerViewController
        newsInnerVC.newsItem = self.news[indexPath.row]
        self.navigationController?.pushViewController(newsInnerVC, animated: true)
    }

}
