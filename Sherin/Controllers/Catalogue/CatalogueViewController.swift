//
//  CatalogueViewController.swift
//  Sherin
//
//  Created by Admin on 5/3/16.
//  Copyright © 2016 mss. All rights reserved.
//

import UIKit
import AKPickerView
import SnapKit
import SDWebImage

class CatalogueViewController: UIViewController, InstanceTrait , AKPickerViewDelegate, AKPickerViewDataSource, UITableViewDelegate, UITableViewDataSource, MenuButtonProtocol, AnalyticsHandler {
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var pickerViewContainer: UIView!
    @IBOutlet weak var tableView: UITableView!
    var brands: [Brand]!
    var currentBrand: Brand!
    var picker = AKPickerView()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.leftBarButtonItem = menuBarButton()
        self.navigationItem.title = locStr("Catalogue")

        let headerView = self.tableView.tableHeaderView
        headerView?.frame.size.height = self.view.frame.height / 3.0
        self.tableView.tableHeaderView = headerView
        
        
        API.getBrands ( { (brands) in
            self.brands = brands.sort({ (prev, post) -> Bool in
                return prev.priority < post.priority
            })
            self.currentBrand = self.brands.first
            self.tableView.delegate = self
            self.tableView.dataSource = self
            self.createPicker()
            self.setData()
            self.tableView.reloadData()
            }, onFailure: {
                reply in
                if let message = reply.message {
                    AlertHelper.presentDefaultAlert(withMessage: message)
                } else {
                    AlertHelper.presentNetworkAlert()
                }
            }
        )
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        clearSelection()
        sendScreenStats()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.currentBrand.types.count
    }

    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(TypeTableViewCell.className(), forIndexPath: indexPath) as! TypeTableViewCell

        configureCell(cell, forRow: indexPath.row)

        return cell
    }
    
    func configureCell(cell: TypeTableViewCell, forRow row: Int) {
        cell.titleLabel.textColor = UIColor.appRedColor()
        cell.titleLabel.text = currentBrand.types[row].name
        if let imageURL = NSURL(string: API.type_img + currentBrand.types[row].image) where currentBrand.types[row].image != "" {
            cell.typeImageView.sd_setImageWithURL(imageURL)
        } else {
            cell.typeImageView.image = UIImage(named: "logo")
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let typeViewController = TypeViewController.instance() as! TypeViewController
        typeViewController.type = currentBrand.types[indexPath.row]
        navigationController?.pushViewController(typeViewController, animated: true)
    }
    
//    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        return self.headerView
//    }
//    
//    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        return self.view.frame.height / 3.0
//    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func setData() {
        if let bgImageURL = NSURL(string: API.brand_bg + self.currentBrand.background) where self.currentBrand.background != "" {
            self.backgroundImageView.sd_setImageWithURL(bgImageURL)
        } else {
            self.backgroundImageView.image = UIImage(named: "brand_bg")
        }
        if let logoImageURL = NSURL(string: API.brand_logo + self.currentBrand.logo) where self.currentBrand.logo != "" {
            self.logoImageView.sd_setImageWithURL(logoImageURL)
        } else {
            self.logoImageView.image = UIImage(named: "logo")
        }
    }
    
    // Picker View
    
    func createPicker() {
        
        picker.delegate = self
        picker.dataSource = self
        picker.backgroundColor = UIColor.appRedColor()
        picker.pickerViewStyle = .StyleFlat
        picker.textColor = UIColor.lightGrayColor()
        picker.highlightedTextColor = UIColor.whiteColor()
        picker.interitemSpacing = 12.0
        picker.maskDisabled = false
        pickerViewContainer.addSubview(picker)
        picker.snp_makeConstraints { (make) in
            make.edges.equalTo(pickerViewContainer).inset(UIEdgeInsetsZero)
        }
        picker.reloadData()
        
    }
    
    func numberOfItemsInPickerView(pickerView: AKPickerView!) -> UInt {
        return UInt(self.brands.count)
    }
    
    func pickerView(pickerView: AKPickerView!, didSelectItem item: Int) {
        currentBrand = self.brands[item]
        setData()
        self.tableView.reloadData()
    }
    
    func pickerView(pickerView: AKPickerView!, titleForItem item: Int) -> String! {
        let title = self.brands[item].name
        return title.uppercaseString
        
    }
    
    //
    
    func clearSelection() {
        if let selectedIndexPath = tableView.indexPathForSelectedRow {
            tableView.deselectRowAtIndexPath(selectedIndexPath, animated: true)
        }
    }

}
