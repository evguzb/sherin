//
//  ProductViewController.swift
//  Sherin
//
//  Created by admin on 6/24/16.
//  Copyright © 2016 mss. All rights reserved.
//

import UIKit

class ProductViewController: UITableViewController, InstanceTrait {
    
    var productIndex: Int!
    var product: Product!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var likeDescriptionLabel: UILabel!
    @IBOutlet weak var ContentLabel: UILabel!
    @IBOutlet weak var likeCountLabel: UILabel!
    @IBOutlet weak var productImageView: UIImageView!
    var delegate: ProductControllerUnwindDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = self.view.frame.height
        self.setupUIElements()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        delegate.unwindFromProductWith(product, atIndex: productIndex)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        /*
        switch segue.identifier {
            case "UnwindWithEditedProductID":
            
        default:
            break
        }
         */
    }
 
    
    // MARK: - UI
    
    func setupUIElements() {
        likeDescriptionLabel.text = locStr("PeopleLikeIt")
        likeCountLabel.text = String(self.product.likes)
        descriptionLabel.text = locStr("Description")
        ContentLabel.text = self.product.description
        if let url = NSURL(string: API.product_img + self.product.image) where self.product.image != "" {
            productImageView.sd_setImageWithURL(url)
        } else {
            productImageView.image = UIImage(named: "logo")
        }
        
        switch product.hasLike {
        case false:
            likeButton.setImage(UIImage(named: "heart_empty"), forState: .Normal)
        case true:
            likeButton.setImage(UIImage(named: "heart_filled"), forState: .Normal)
        }
    }
    
    @IBAction func likeTapped(sender: UIButton) {
        UIView.animateWithDuration(0.3, animations: {
            self.likeButton.transform = CGAffineTransformMakeScale(0.1, 0.1)
        }) { (finished) in
            self.product.sendChangeLike({
                // onSuccess
                self.setupUIElements()
                UIView.animateWithDuration(0.3, animations: {
                    self.likeButton.transform = CGAffineTransformMakeScale(1, 1)
                })
                
                // onFailure:
            }) {
                UIView.animateWithDuration(0.3, animations: {
                    self.likeButton.transform = CGAffineTransformMakeScale(1, 1)
                })
            }
        }
    }
}
