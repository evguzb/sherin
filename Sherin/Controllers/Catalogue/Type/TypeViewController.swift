//
//  TypeViewController.swift
//  Sherin
//
//  Created by admin on 6/23/16.
//  Copyright © 2016 mss. All rights reserved.
//

import UIKit

protocol ProductControllerUnwindDelegate {
    func unwindFromProductWith(product: Product, atIndex index: Int)
}

class TypeViewController: UITableViewController, InstanceTrait, ProductControllerUnwindDelegate {
    
    var type: Type!
    var products = [Product]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.clearsSelectionOnViewWillAppear = true
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 80.0
        
        self.type.requestProducts({ (products) in
            self.products = products
            self.tableView.reloadData()
            }) { (reply) in
                if let message = reply.message {
                    AlertHelper.presentDefaultAlert(withMessage: message)
                } else {
                    AlertHelper.presentNetworkAlert()
                }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.products.count + 1
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell: UITableViewCell
        switch indexPath.row {
        case 0:
            cell = infoCell()
        default:
            cell = sausageCell(forIndexPath: indexPath)
        }
        return cell
    }
    
    func infoCell() -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(InfoTableViewCell.className()) as! InfoTableViewCell
        cell.contentLabel.text = self.type.description
        return cell
    }
    
    func sausageCell(forIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(SausageTableViewCell.className(), forIndexPath: indexPath) as! SausageTableViewCell
        configureSausageCell(cell, row: indexPath.row - 1)
        return cell
    }
    
    // Row here is index within Products array
    func configureSausageCell(cell: SausageTableViewCell, row: Int) {
        let product = self.products[row]
        cell.moreLabel.text = locStr("InDetails")
        cell.brandLabel.text = self.type.brand?.name.uppercaseString ?? ""
        cell.nameLabel.text = product.name
        cell.likeCountLabel.text = String(product.likes)
        if let url = NSURL(string: API.product_img + product.image) where product.image != "" {
            cell.photoImageView.sd_setImageWithURL(url)
        } else {
            cell.photoImageView.image = UIImage(named: "logo")
        }
        switch product.hasLike {
        case false:
            cell.likeButton.setImage(UIImage(named: "heart_empty"), forState: .Normal)
        case true:
            cell.likeButton.setImage(UIImage(named: "heart_filled"), forState: .Normal)
        }
        cell.likeButton.tag = row + 1
        
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        switch indexPath.row {
        case 0:
            break
        default:
            let productVC = ProductViewController.instance() as! ProductViewController
            productVC.delegate = self
            productVC.product = self.products[indexPath.row - 1]
            productVC.productIndex = indexPath.row - 1
            self.navigationController?.pushViewController(productVC, animated: true)
        }
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
 
    
    // MARK: - Unwind
    
    func unwindFromProductWith(product: Product, atIndex index: Int) {
        print("inTYPE")
        self.products[index] = product
        self.tableView.reloadData()
    }
    
    
    // MARK: - Actions
    
    @IBAction func likeTapped(sender: UIButton) {
        let cell = self.tableView.cellForRowAtIndexPath(NSIndexPath(forRow: sender.tag, inSection: 0)) as! SausageTableViewCell
        let product = self.products[sender.tag - 1]
        UIView.animateWithDuration(0.3, animations: {
            cell.likeButton.transform = CGAffineTransformMakeScale(0.1, 0.1)
            }) { (finished) in
                product.sendChangeLike({
                    // onSuccess
                    self.configureSausageCell(cell, row: sender.tag - 1)
                    UIView.animateWithDuration(0.3, animations: {
                        cell.likeButton.transform = CGAffineTransformMakeScale(1, 1)
                    })
 
                    // onFailure:
                }) {
                    UIView.animateWithDuration(0.3, animations: {
                        cell.likeButton.transform = CGAffineTransformMakeScale(1, 1)
                    })
                }
        }
        
    }
}
