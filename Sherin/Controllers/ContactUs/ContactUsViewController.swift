//
//  ContactUsViewController.swift
//  Sherin
//
//  Created by Admin on 5/3/16.
//  Copyright © 2016 mss. All rights reserved.
//

import UIKit
import MessageUI

class ContactUsViewController: UIViewController, InstanceTrait, MenuButtonProtocol {

    @IBOutlet weak var topicLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var topicTextField: UITextField!
    @IBOutlet weak var messageTextView: UITextView!
    @IBOutlet weak var sendButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.leftBarButtonItem = menuBarButton()
        topicLabel.text = locStr("Topic").uppercaseString
        messageLabel.text = locStr("Message").uppercaseString
        sendButton.setTitle(locStr("Send"), forState: .Normal)
        self.messageTextView.layer.borderWidth = 1.0
        self.messageTextView.layer.borderColor = UIColor.lightGrayColor().CGColor
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.topicTextField.resignFirstResponder()
        self.messageTextView.resignFirstResponder()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - Actions
    
    @IBAction func sendTapped(sender: UIButton) {
        if isDataReady() {
            API.sendContact(title: self.topicTextField.text!, content: self.messageTextView.text!, onSuccess: { (reply) in
                if reply.status {
                    self.topicTextField.text = ""
                    self.messageTextView.text = ""
                    AlertHelper.presentDefaultAlert(withMessage: locStr("ContactSendSuccess"))
                } else {
                    AlertHelper.presentDefaultAlert(withMessage: locStr("ContactSendFail"))
                }
                }, onFailure: { (reply) in
                    AlertHelper.presentDefaultAlert(withMessage: reply.message ?? locStr("ContactSendFail"))
            })
        } else {
            AlertHelper.presentDefaultAlert(withMessage: locStr("FieldsNotReady"))
        }
    }
    
    // MARK: - Helpers
    
    func isDataReady() -> Bool {
        if self.topicTextField.text != "" && self.messageTextView.text != "" {
            return true
        }
        return false
    }
}
