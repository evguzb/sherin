//
//  Offer.swift
//  Sherin
//
//  Created by admin on 7/7/16.
//  Copyright © 2016 mss. All rights reserved.
//

import Foundation

class Offer: ModelListable {
    var id: Int
    var message: String
    var date: NSDate
    
    required init(dict: [String : AnyObject], parent: ModelListable?) {
        self.id = Int(dict["push_id"] as! String)!
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = API.dateFormat
        self.date = dateFormatter.dateFromString(dict["create_at"] as! String)!
        let dataStr = dict["push_data"] as! String
        let data = dataStr.dataUsingEncoding(NSUTF8StringEncoding)!
        if let dataDict = try? NSJSONSerialization.JSONObjectWithData(data, options: .MutableContainers) {
            self.message = dataDict["message"] as! String
        } else {
            self.message = "" }
    }
}