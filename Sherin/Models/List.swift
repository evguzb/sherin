//
//  List.swift
//  Sherin
//
//  Created by admin on 6/10/16.
//  Copyright © 2016 mss. All rights reserved.
//

import Foundation

protocol ModelListable {
    init(dict: [String: AnyObject], parent: ModelListable?)
}

class List < T: ModelListable > {
    var items : [T] = []
    
    //init(fromReply: )
    
    required init(array: [[String: AnyObject]], parent: ModelListable?) {
        for item in array {
            items.append(T(dict: item, parent: parent))
        }
        
    }
    
    static func initWith(reply: Reply, parent: ModelListable?) -> List {
        return List(array: reply.data["list"] as! [[String: AnyObject]], parent: parent)
    }
}