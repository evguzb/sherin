//
//  MapLocation.swift
//  Sherin
//
//  Created by Olimjon Kenjaev on 6/30/16.
//  Copyright © 2016 mss. All rights reserved.
//

import UIKit
import MapKit

class MapLocation: NSObject, ModelListable, MKAnnotation {
    var categoryId:String
    var classId:String
    var outletAddressRu:String
    var outletAddressUz:String
    var outletId:String
    var outletLatitude:Double
    var outletLongitude:Double
    var outletNameRu:String
    var outletNameUz:String
    var outletPhone:String
    var outletState:String
    var salesChannelId:String
    var typeId:String

    required init(dict: [String : AnyObject], parent: ModelListable?) {
        self.categoryId = dict["category_id"] as! String
        self.classId = dict["class_id"] as! String
        self.outletAddressRu = dict["outlet_address_ru"] as! String
        self.outletAddressUz = dict["outlet_address_uz"] as! String
        self.outletId = dict["outlet_id"] as! String
        self.outletLatitude = Double((dict["outlet_latitude"] as! String))!
        self.outletLongitude = Double((dict["outlet_longitude"] as! String))!
        self.outletNameRu = dict["outlet_name_ru"] as! String
        self.outletNameUz = dict["outlet_name_uz"] as! String
        self.outletPhone = dict["outlet_phone"] as! String
        self.outletState = dict["outlet_state"] as! String
        self.salesChannelId = dict["sales_channel_id"] as! String
        self.typeId = dict["type_id"] as! String
    }

    //MKAnnotation
    var coordinate: CLLocationCoordinate2D {
        get {
            return CLLocationCoordinate2D(latitude: self.outletLatitude, longitude: self.outletLongitude);
        }
    }

    @objc var title: String? {
        get {
            return outletNameUz;
        }
    }

    @objc var subtitle: String? {
        get {
            return ""
        }
    }
    
    
}
