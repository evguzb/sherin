//
//  AdviceImage.swift
//  Sherin
//
//  Created by admin on 7/8/16.
//  Copyright © 2016 mss. All rights reserved.
//

import Foundation

class AdviceImage: Image {
    var id: Int
    var img: String
    required init(dict: [String : AnyObject], parent: ModelListable?) {
        self.id = Int(dict["id"] as! String)!
        self.img = dict["advice_img"] as! String
    }
}