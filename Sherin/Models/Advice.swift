//
//  Advice.swift
//  Sherin
//
//  Created by admin on 7/5/16.
//  Copyright © 2016 mss. All rights reserved.
//

import Foundation

class Advice: ModelListable {
    let id: Int
    let title: String
    let content: String
    let previewImg: String
    let date: NSDate
    let images: [AdviceImage]
    
    required init(dict: [String : AnyObject], parent: ModelListable?) {
        let lang = LocalizeHelper.sharedLocale.lang.rawValue
        self.id = Int(dict["advice_id"] as! String)!
        self.title = dict["advice_title_" + lang] as! String
        self.content = dict["advice_content_" + lang] as! String
        self.previewImg = dict["advice_preview"] as! String
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = API.dateFormat
        self.date = dateFormatter.dateFromString(dict["created_at"] as! String)!
        self.images = List<AdviceImage>(array: dict["adviceImages"] as! [[String: AnyObject]], parent: nil).items
    }
}