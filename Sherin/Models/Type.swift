//
//  Type.swift
//  Sherin
//
//  Created by admin on 6/10/16.
//  Copyright © 2016 mss. All rights reserved.
//

import Foundation

class Type: ModelListable {
    let id: Int
    let name: String
    let productLine: String
    let image: String
    let description: String
    weak var brand: Brand?
    
    required init(dict: [String : AnyObject], parent: ModelListable?) {
        let lang = LocalizeHelper.sharedLocale.lang.rawValue
        self.id = Int(dict["type_id"] as! String)!
        self.name = dict["type_name_" + lang] as! String
        self.productLine = dict["product_line_" + lang] as! String
        self.image = dict["type_img"] as! String
        self.description = dict["desc_" + lang] as! String
        self.brand = parent as? Brand
    }
    
    func requestProducts(onSuccess: [Product] -> Void, onFailure: Reply -> Void) {
        let request = Request(type: .products)
        request.setParam(self.id, forKey: "type_id")
        if UserData.sharedData.role == .guest {
            request.setParam(UserData.sharedData.server_device_token!, forKey: "server_device_token")
        } else {
            request.setParam(UserData.sharedData.id!, forKey: "id")
            request.setParam(UserData.sharedData.token!, forKey: "token")
        }
        
        API.executeRequest(request) { (reply) in
            if reply.status {
                let products = List<Product>.initWith(reply, parent: self).items
                onSuccess(products)
            } else {
                onFailure(reply)
            }
        }
    }
}