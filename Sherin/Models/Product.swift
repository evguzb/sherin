//
//  Product.swift
//  Sherin
//
//  Created by admin on 6/24/16.
//  Copyright © 2016 mss. All rights reserved.
//

import Foundation

class Product: ModelListable {
    let id: Int
    let name: String
    var type: Type?
    let image: String
    let description: String
    var likes: Int
    var hasLike: Bool
    
    required init(dict: [String: AnyObject], parent: ModelListable?) {
        let lang = LocalizeHelper.sharedLocale.lang.rawValue
        self.id = Int((dict["product_id"] as! String))!
        self.name = dict["product_name_" + lang] as! String
        self.description = dict["product_desc_" + lang] as! String
        self.likes = Int(dict["product_likes"] as! String)!
        self.image = dict["product_img"] as! String
        let hasLikeInt = Int(dict["has_like"] as! String)!
        if hasLikeInt == 0 {
            self.hasLike = false
        } else {
            self.hasLike = true
        }
        self.type = parent as? Type
    }
    
    func sendChangeLike(onSuccess: () -> Void, onFailure: () -> Void) {
        if self.hasLike {
            self.sendDislike(onSuccess, onFailure: onFailure)
        } else {
            self.sendLike(onSuccess, onFailure: onFailure)
        }
    }
    
    func sendLike(onSuccess: () -> Void, onFailure: () -> Void) {
        let request = Request(type: .like)
        request.setParam(self.id, forKey: "product_id")
        if UserData.sharedData.role == .guest {
            request.setParam(UserData.sharedData.server_device_token!, forKey: "server_device_token")
        } else {
            request.setParam(UserData.sharedData.id!, forKey: "id")
            request.setParam(UserData.sharedData.token!, forKey: "token")
        }
        API.executeRequestWithoutProgress(request) { (reply) in
            if reply.status {
                self.hasLike = true
                self.likes += 1
                onSuccess()
            } else {
                onFailure()
            }
        }
        
    }
    
    func sendDislike(onSuccess: () -> Void, onFailure: () -> Void) {
        let request = Request(type: .dislike)
        request.setParam(self.id, forKey: "product_id")
        if UserData.sharedData.role == .guest {
            request.setParam(UserData.sharedData.server_device_token!, forKey: "server_device_token")
        } else {
            request.setParam(UserData.sharedData.id!, forKey: "id")
            request.setParam(UserData.sharedData.token!, forKey: "token")
        }
        API.executeRequestWithoutProgress(request) { (reply) in
            if reply.status {
                self.hasLike = false
                self.likes -= 1
                onSuccess()
            } else {
                onFailure()
            }
        }
    }
}