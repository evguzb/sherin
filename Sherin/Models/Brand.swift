
//
//  Brand.swift
//  Sherin
//
//  Created by admin on 6/10/16.
//  Copyright © 2016 mss. All rights reserved.
//

import Foundation

class Brand: ModelListable {
    let id: Int
    let name: String
    let priority: Int
    let logo: String
    let background: String
    let types: [Type]
    
    required init (dict: [String: AnyObject], parent: ModelListable?) {
        self.id = Int(dict["brand_id"] as! String)!
        self.name = dict["brand_name_" + LocalizeHelper.sharedLocale.lang.rawValue] as! String
        self.priority = Int(dict["brand_priority"] as! String)!
        self.logo = dict["brand_logo"] as! String
        self.background = dict["brand_bg"] as! String
        self.types = List<Type>(array: dict["types"] as! [[String: AnyObject]], parent: nil).items
        for type in types {
            type.brand = self
        }
    }
}