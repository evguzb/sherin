//
//  Image.swift
//  Sherin
//
//  Created by admin on 7/8/16.
//  Copyright © 2016 mss. All rights reserved.
//

import Foundation

protocol Image: ModelListable {
    var id: Int {get set}
    var img: String {get set}
}