//
//  NewsItem.swift
//  Sherin
//
//  Created by admin on 7/5/16.
//  Copyright © 2016 mss. All rights reserved.
//

import Foundation

class NewsItem: ModelListable {
    
    let id: Int
    let title: String
    let content: String
    let previewImg: String
    let date: NSDate
    let images: [NewsImage]
    
    required init(dict: [String : AnyObject], parent: ModelListable?) {
        let lang = LocalizeHelper.sharedLocale.lang.rawValue
        self.id = Int(dict["news_id"] as! String)!
        self.title = dict["news_title_" + lang] as! String
        self.content = dict["news_content_" + lang] as! String
        self.previewImg = dict["news_preview"] as! String
        
        let dateStr = dict["created_at"] as! String
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = API.dateFormat
        self.date = dateFormatter.dateFromString(dateStr)!
        self.images = List<NewsImage>(array: dict["newsImages"] as! [[String: AnyObject]], parent: nil).items
    }
}