//
//  NewsImage.swift
//  Sherin
//
//  Created by admin on 7/20/16.
//  Copyright © 2016 mss. All rights reserved.
//

import Foundation

class NewsImage: Image {
    var id: Int
    var img: String
    required init(dict: [String : AnyObject], parent: ModelListable?) {
        self.id = Int(dict["id"] as! String)!
        self.img = dict["news_img"] as! String
    }
}