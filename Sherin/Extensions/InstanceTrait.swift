//
//  InstanceTrait.swift
//  Sherin
//
//  Created by Admin on 5/2/16.
//  Copyright © 2016 mss. All rights reserved.
//

import UIKit

public protocol InstanceTrait {
    
    static func instance() -> UIViewController
    
}

extension InstanceTrait {
    
    static func instance() -> UIViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewControllerWithIdentifier(String(self))
    }
    
}

