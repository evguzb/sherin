//
//  MenuButton.swift
//  Sherin
//
//  Created by admin on 6/16/16.
//  Copyright © 2016 mss. All rights reserved.
//

import UIKit

protocol MenuButtonProtocol: NSObjectProtocol {
}

extension MenuButtonProtocol where Self : UIViewController {
    func menuBarButton() -> UIBarButtonItem {
        return MenuBarButton(image: UIImage(named: "menu_icon")!, actionHandler: { 
            self.menuContainerViewController.toggleLeftSideMenuCompletion({
            })
        })
    }
}

class MenuBarButton: UIBarButtonItem {
    var actionHandler: (() -> Void)?
    
    convenience init (image: UIImage, actionHandler: (() -> Void)?) {
        self.init(image: image, style: .Plain, target: nil, action:  nil)
        self.target = self
        self.action = #selector(MenuBarButton.menuButtonPressed)
        self.actionHandler = actionHandler
        
    }
    
    func menuButtonPressed() {
        self.actionHandler?()
    }
}