//
//  UIViewController.swift
//  Sherin
//
//  Created by Admin on 5/2/16.
//  Copyright © 2016 mss. All rights reserved.
//

import UIKit

extension UIViewController {
    func className() -> String {
        return String(self)
    }
}
