//
//  UITableViewCell.swift
//  Sherin
//
//  Created by admin on 6/13/16.
//  Copyright © 2016 mss. All rights reserved.
//

import UIKit

extension UITableViewCell {
    static func className() -> String {
        return String(self)
    }
}

extension UICollectionViewCell {
    static func className() -> String {
        return String(self)
    }
}