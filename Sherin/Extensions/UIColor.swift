//
//  UIColor.swift
//  Sherin
//
//  Created by Admin on 5/2/16.
//  Copyright © 2016 mss. All rights reserved.
//

import UIKit

extension UIColor {
    
    static func colorWith(Red r: CGFloat, Green g: CGFloat, Blue b: CGFloat, Alpha a: CGFloat) -> UIColor {
        return UIColor(red: r/255.0, green: g/255.0, blue: b/255.0, alpha: a)
    }
    
    static func colorWith(Red r: CGFloat, Green g: CGFloat, Blue b: CGFloat) -> UIColor {
        return UIColor(red: r/255.0, green: g/255.0, blue: b/255.0, alpha: 1.0)
    }
    
    static func appRedColor() -> UIColor {
        return UIColor.colorWith(Red: 191.0, Green: 45, Blue: 33)
    }
    
}

