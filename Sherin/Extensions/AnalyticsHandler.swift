//
//  AnalyticsHandler.swift
//  Sherin
//
//  Created by admin on 7/4/16.
//  Copyright © 2016 mss. All rights reserved.
//

import UIKit

var screenNameForType: [String: String] = [
    "DashboardViewController": "Dashboard",
    "CatalogueViewController": "Catalogue",
    "NewsViewController": "News",
    "MapViewController": "Map",
    "AdvicesViewController": "Advices",
    "SuggestionViewController": "Offers"
]

protocol AnalyticsHandler {
    func sendScreenStats()
}

extension AnalyticsHandler where Self : UIViewController {
    func sendScreenStats() {
        let tracker = GAI.sharedInstance().defaultTracker
        tracker.set(kGAIScreenName, value: screenNameForType[String(self.dynamicType)]!)
        print(screenNameForType[String(self.dynamicType)]!)
        let builder = GAIDictionaryBuilder.createScreenView()
        tracker.send(builder.build() as [NSObject : AnyObject])
    }
}