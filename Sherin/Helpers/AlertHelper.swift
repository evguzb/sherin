//
//  AlertHelper.swift
//  Sherin
//
//  Created by admin on 6/23/16.
//  Copyright © 2016 mss. All rights reserved.
//

import UIKit

class AlertHelper {
    
    static func presentNetworkAlert() {
        presentDefaultAlert(withMessage: locStr("NetworkAlertMessage"))
    }
    
    static func presentDefaultAlert(withMessage message: String) {
        let okAction = UIAlertAction(title: locStr("OK"), style: .Default, handler: nil)
        presentAlert(withTitle: nil, message: message, actions: [okAction])
    }
    
    static func presentAlert(withTitle title: String?, message: String, actions: [UIAlertAction]) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        for action in actions {
            alertController.addAction(action)
        }
        (UIApplication.sharedApplication().delegate as! AppDelegate).window?.rootViewController?.presentViewController(alertController, animated: true, completion: nil)
    }
    
}