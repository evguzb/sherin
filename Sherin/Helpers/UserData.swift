//
//  UserData.swift
//  Sherin
//
//  Created by Admin on 5/3/16.
//  Copyright © 2016 mss. All rights reserved.
//

import UIKit

enum UserRole: String {
    case guest = "guest"
    case client = "client"
    case seller = "seller"
}

class UserData {
    static let sharedData = UserData()
    var role: UserRole
    var name: String?
    var id: Int?
    var email: String?
    var token: String?
    var server_device_token: String?
    var device_type = "ios"
    var device_token: String?

    var deviceToken: String {
        get {
            return device_token ?? "e0a8347e38eb553c16d44065cda82bfaf079757babe4e27e2113693e51033238";
        }
    }

    init() {
        let defaults = NSUserDefaults.standardUserDefaults()
        if let role = defaults.stringForKey("user_role"), let serverToken = defaults.stringForKey("server_device_token") {
            self.server_device_token = serverToken
            self.role = UserRole(rawValue: role)!
            if self.role != .guest {
                self.name = defaults.stringForKey("user_name")
                self.id = defaults.integerForKey("user_id")
                self.email = defaults.stringForKey("user_email")
                self.token = defaults.stringForKey("user_token")
            }
        } else {
            self.role = .guest
        }
    }
    
    func setDeviceData(fromReply reply: Reply) {
        self.role = .guest
        self.server_device_token = reply.data["server_device_token"] as? String
        saveDataToUserDefaults()
    }
    
    func setUserData(fromReply reply: Reply) {
        let dict = reply.data
        if dict["role"] as! String == "admin" {
            self.role = .guest
        } else {
            self.role = UserRole(rawValue: dict["role"] as! String)!
        }
        self.name = dict["user_name"] as? String
        self.id = dict["id"] as? Int
        self.email = dict["email"] as? String
        self.token = dict["token"] as? String
        saveDataToUserDefaults()
    }
    
    func saveDataToUserDefaults() {
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.setObject(role.rawValue, forKey: "user_role")
        defaults.setObject(server_device_token, forKey: "server_device_token")
        if role != .guest {
            defaults.setObject(name, forKey: "user_name")
            defaults.setInteger(id!, forKey: "user_id")
            defaults.setObject(email, forKey: "user_email")
            defaults.setObject(token, forKey: "user_token")
        }
        
    }
    
    func dropData() {
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.setObject(UserRole.guest.rawValue, forKey: "user_role")
        self.role = .guest
        self.id = nil
        self.name = nil
        self.email = nil
        self.token = nil
    }
}