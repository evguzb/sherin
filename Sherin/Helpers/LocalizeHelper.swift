//
//  LocalizeHelper.swift
//  Sherin
//
//  Created by Admin on 5/2/16.
//  Copyright © 2016 mss. All rights reserved.
//

import Foundation

enum Language: String {
    case russian = "ru"
    case uzbek = "uz"
}

class LocalizeHelper {
    static let sharedLocale = LocalizeHelper()
    var bundle: NSBundle
    var lang: Language
    
    init() {
        
        if let langStr = NSUserDefaults.standardUserDefaults().stringForKey("lang") {
            self.lang = Language(rawValue: langStr)!
            let path = NSBundle.mainBundle().pathForResource(langStr, ofType: "lproj")!
            self.bundle = NSBundle(path: path)!
        } else {
            self.bundle = NSBundle(path: Language.russian.rawValue) ?? NSBundle.mainBundle()
            self.lang = .russian
        }
    }
    
    func setLanguage(language: Language) {
        let path = NSBundle.mainBundle().pathForResource(language.rawValue, ofType: "lproj") ?? "ru"
        self.bundle = NSBundle(path: path) ?? NSBundle.mainBundle()
    }
    
    func localizedStringForKey(key: String) -> String {
        return self.bundle.localizedStringForKey(key, value: "NoLocalizedString", table: nil)
    }
}

func locStr(key: String) -> String {
    return LocalizeHelper.sharedLocale.localizedStringForKey(key)
}